console.log('hello')
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')

const app = express()
app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors()) // Enabled for dev (Will remove in production)

app.get('/status', (req, res) => {
  res.send({
    message: 'hello world'
  })
})
app.listen(process.env.PORT || 3000)
