import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home'
import Music from '@/components/Music'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/music',
      name: 'music',
      component: Music
    }
  ]
})
